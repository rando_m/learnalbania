var microphone = false,
    camera = false,
    fullscreen = false;
var domain = 'https://app.learnalbanian.al';
// var domain = 'http://localhost/learnalbania/public/index.php';
$(document).ready(function() {
    $('#publisher').draggable();

    $('.end-call').on('click',function(event){
        session.disconnect();
        $.ajax({
            url:domain+'/archive/stop/' + sessionId,
            method:'POST',
            data:{'archiveid':archiveID},
            success:function(response){
                if(typeof response['redirect'] !== 'undefined' && response['redirect']!=''){
                    window.location = response['redirect'];
                }
            }
        });

    });

    $('.microphone').on('click',function(event){
        publisher.publishAudio(microphone);
        if(microphone == false){
            microphone = true;
            $(this).find('.off').show();
            $(this).find('.on').hide();
        }
        else{
            microphone = false;
            $(this).find('.off').hide();
            $(this).find('.on').show();
        }
    });

    $('.video-call').on('click',function(event){
        publisher.publishVideo(camera);
        if(camera == false){
            camera = true;
            $(this).find('.off').show();
            $(this).find('.on').hide();
        }
        else{
            camera = false;
            $(this).find('.off').hide();
            $(this).find('.on').show();
        }
    });

    $('.keyboard').on('click',function(event){
        $('#sb_chat').toggleClass('show');
        $('.keyboard').removeClass('dot');
    });

    $('.resize-icon').on('click',function(e){
        var elem = document.documentElement;
        if(fullscreen == false){
            openFullscreen(elem);
            fullscreen = true;
        }
        else{
            closeFullscreen(elem);
            fullscreen = false;
        }
    });

    var sb = new SendBird({appId: sendbirdkey});
    sb.connect(userId, function(user, error) {
        if (error) {
            console.log(error);
            return;
        }
        if(chaturl == ''){
            sb.OpenChannel.createChannel(channel_name, '', '', '', '', function(channel, error) {
                if (error) {
                    console.log(error);
                    return;
                }
                $.ajax({
                    url: "/chat/start",
                    type:'POST',
                    data:{'sessionId':sessionId,'chaturl':channel.url}

                });
                liveChat.startWithConnect(sendbirdkey, userId, nickname, function() {
                    liveChat.enterChannel(channel.url, function() {
                    });
                });
            });
        }
        else{
            liveChat.startWithConnect(sendbirdkey, userId, nickname, function() {
                liveChat.enterChannel(chaturl, function() {
                });
            });
        }
        var ChannelHandler = new sb.ChannelHandler();
        ChannelHandler.onMessageReceived = function(channel, message) {
            if(!$('#sb_chat').hasClass('show')){
                $('.keyboard').addClass('dot');
            }
            $.notify({
                // options
                icon: 'glyphicon glyphicon-warning-sign',
                // title: 'Bootstrap notify',
                message: message.message,
                // url: 'https://github.com/mouse0270/bootstrap-notify',
                // target: '_blank'
            }, {
                // settings
                element: 'body',
                position: null,
                type: "info",
                allow_dismiss: true,
                newest_on_top: false,
                showProgressbar: false,
                placement: {
                    from: "top",
                    align: "right"
                },
                offset: 20,
                spacing: 10,
                z_index: 1031,
                delay: 4000,
                timer: 1000,
                url_target: '_blank',
                mouse_over: null,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },
                onShow: null,
                onShown: null,
                onClose: null,
                onClosed: null,
                icon_type: 'class',
                template: '<div data-notify="container" class="chat-notification col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                // '<span data-notify="icon"></span> ' +
                // '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                // '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
            });
        };
        sb.addChannelHandler(userId, ChannelHandler);
    });

    var checkExist = setInterval(function() {
        if ($('#sb_chat .message-board .content').length) {
            var window_height = ($(window).height() - 100) * 0.7;
            $('#sb_chat .message-board .content').css({'max-height':window_height+'px'});
            clearInterval(checkExist);
        }
    }, 100); // check every 100ms
    $(window).on('resize',function(){
        var checkExist = setInterval(function() {
            if ($('#sb_chat .message-board .content').length) {
                var window_height = ($(window).height() - 100) * 0.7;
                $('#sb_chat .message-board .content').css({'max-height':window_height+'px'});
                clearInterval(checkExist);
            }
        }, 100); // check every 100ms
    });
    $(document).on('click','.chat-notification',function(e){
        $('#sb_chat').toggleClass('show');
        $('.keyboard').removeClass('dot');
    });
});

/* View in fullscreen */
function openFullscreen(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
    return true;
}

/* Close fullscreen */
function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

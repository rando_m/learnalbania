var domain = 'https://app.learnalbanian.al';
// var domain = 'http://localhost/learnalbania/public/index.php';
$(document).on('click','#submit-email',function(e){
    e.preventDefault();
    var email2 = $('#email').val();
    $.ajax({
        url:domain+'/validate/teacher/'+sessionId,
        method:'POST',
        data:{ email:email2 },
        success:function(response){
            if(response['errors'] != ''){
                $('#teacher-validate .error').html(response['errors']);
            }
            if(typeof response['redirect'] !== 'undefined' && response['redirect']!=''){
                window.location = response['redirect'];
            }
        }
    })
});
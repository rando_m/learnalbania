var session = OT.initSession(apiKey, sessionId),
    publisher = OT.initPublisher('publisher'),
    archiveID = null;
session.connect(token, function(error) {
  if (error) {
    console.error('Failed to connect', error);
  } else {
    session.publish(publisher, function(error) {
      if (error) {
        console.error('Failed to publish', error);
      }
    });
  }
});

session.on('streamCreated', function(event) {
  session.subscribe(event.stream, 'subscribers', {
    insertMode: 'append'
  }, function(error) {
    if (error) {
      console.error('Failed to subscribe', error);
    }
  });
    $('#publisher').prependTo('#bottom-bar');
    $('#subscribers h3').toggleClass('hidden');
    $.ajax({
        url:domain+'/student/info/'+sessionId,
        method:'POST',
        success:function(response){
            if(typeof response['first_name'] !== 'undefined' && response['first_name']!=''){
                $.notify({
                    // options
                    icon: 'glyphicon glyphicon-warning-sign',
                    // title: 'Bootstrap notify',
                    message: response['first_name']+' joined the classroom.',
                    // url: 'https://github.com/mouse0270/bootstrap-notify',
                    // target: '_blank'
                }, {
                    // settings
                    element: 'body',
                    position: null,
                    type: "info",
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: 4000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: null,
                    onShown: null,
                    onClose: null,
                    onClosed: null,
                    icon_type: 'class',
                    template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    // '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    // '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
                });
            }
        },
    });
});
session.on('streamDestroyed', function(event) {
    $('#publisher').appendTo('#subscribers');
    $('#subscribers h3').toggleClass('hidden');
    $.ajax({
        url:domain+'/student/info/'+sessionId,
        method:'POST',
        success:function(response){
            if(typeof response['first_name'] !== 'undefined' && response['first_name']!=''){
                $.notify({
                    // options
                    icon: 'glyphicon glyphicon-warning-sign',
                    // title: 'Bootstrap notify',
                    message: response['first_name']+' left the classroom.',
                    // url: 'https://github.com/mouse0270/bootstrap-notify',
                    // target: '_blank'
                }, {
                    // settings
                    element: 'body',
                    position: null,
                    type: "info",
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: 4000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: null,
                    onShown: null,
                    onClose: null,
                    onClosed: null,
                    icon_type: 'class',
                    template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    // '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    // '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
                });
            }
        },
    });
    // if(archiveID != null) {
    //     $.get('/learnalbania/public/archive/stop/' + sessionId+'/'+archiveID);
    // }
});

session.on('archiveStarted', function(event) {
  archiveID = event.id;
  console.log('ARCHIVE STARTED');
  $('.start').hide();
  $('.stop').show();
  disableForm();
});

session.on('archiveStopped', function(event) {
  archiveID = null;
  console.log('ARCHIVE STOPPED');
  $('.start').show();
  $('.stop').hide();
  enableForm();
});



function disableForm() {
  $('.archive-options-fields').attr('disabled', 'disabled');
}

function enableForm() {
  $('.archive-options-fields').removeAttr('disabled');
}


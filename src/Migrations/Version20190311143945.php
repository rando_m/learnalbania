<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190311143945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sessions ADD chaturl VARCHAR(255) DEFAULT NULL, ADD status VARCHAR(255) DEFAULT NULL, CHANGE starttime starttime DATETIME DEFAULT NULL, CHANGE endtime endtime DATETIME DEFAULT NULL, CHANGE duration duration VARCHAR(100) DEFAULT NULL, CHANGE recordingurl recordingurl VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sessions DROP chaturl, DROP status, CHANGE starttime starttime DATETIME NOT NULL, CHANGE endtime endtime DATETIME NOT NULL, CHANGE duration duration VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE recordingurl recordingurl VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}

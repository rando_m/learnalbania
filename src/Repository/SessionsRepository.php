<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/7/19
 * Time: 1:14 AM
 */

namespace App\Repository;

use App\Entity\Sessions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SessionsRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, Sessions::class);
  }

  public function findOneByTeacher($user,$sessionid)
  {
    return $this->createQueryBuilder('c')
      ->andWhere('c.teacher = :user')
      ->andWhere('c.sessionid = :sessionid')
      ->setParameter('user', $user)
      ->setParameter('sessionid',$sessionid)
      ->setMaxResults(1)
      ->getQuery()
      ->getOneOrNullResult()
      ;
  }

  public function findOneByStudent($user,$sessionid)
  {
    return $this->createQueryBuilder('c')
      ->andWhere('c.student = :user')
      ->andWhere('c.sessionid = :sessionid')
      ->setParameter('user', $user)
      ->setParameter('sessionid',$sessionid)
      ->setMaxResults(1)
      ->getQuery()
      ->getOneOrNullResult()
      ;
  }

  public function findOneBySession($session){
    return $this->createQueryBuilder('c')
      ->andWhere('c.sessionid = :sessionid')
      ->setParameter('sessionid',$session)
      ->setMaxResults(1)
      ->getQuery()
      ->getOneOrNullResult();
  }
}
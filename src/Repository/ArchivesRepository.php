<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/13/19
 * Time: 11:02 AM
 */

namespace App\Repository;

use App\Entity\Archives;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class ArchivesRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, Archives::class);
  }

  public function findOneById($session,$archiveid)
  {
    return $this->createQueryBuilder('c')
      ->andWhere('c.sessionid = :session')
      ->andWhere('c.archiveid = :archiveid')
      ->setParameter('session', $session)
      ->setParameter('archiveid',$archiveid)
      ->setMaxResults(1)
      ->getQuery()
      ->getOneOrNullResult()
      ;
  }
}
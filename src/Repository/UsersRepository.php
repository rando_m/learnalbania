<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/5/19
 * Time: 11:33 PM
 */

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UsersRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, Users::class);
  }

  public function findOneByEmailTeacher($value)
  {
    return $this->createQueryBuilder('c')
      ->andWhere('c.email = :val')
      ->andWhere('c.role = :role')
      ->setParameter('val', $value)
      ->setParameter('role','teacher')
      ->getQuery()
      ->getOneOrNullResult()
      ;
  }

  public function findOneByEmailStudent($value)
  {
    return $this->createQueryBuilder('c')
      ->andWhere('c.email = :val')
      ->andWhere('c.role = :role')
      ->setParameter('val', $value)
      ->setParameter('role','student')
      ->getQuery()
      ->getOneOrNullResult()
      ;
  }
}
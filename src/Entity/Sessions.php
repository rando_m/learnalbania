<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/5/19
 * Time: 10:29 PM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionsRepository")
 */
class Sessions
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $sessionid;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\Users")
   */
  private $student;

  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\Users")
   * @JoinColumn(name="teacher_id", referencedColumnName="id")
   */
  private $teacher;

  /**
   * @ORM\Column(type="datetime",nullable=true)
   */
  private $starttime;

  /**
   * @ORM\Column(type="datetime",nullable=true)
   */
  private $endtime;

  /**
   * @ORM\Column(type="string", length=100,nullable=true)
   */
  private $duration;

  /**
   * @ORM\Column(type="string", length=255,nullable=true)
   */
  private $recordingurl;

  /**
   * @ORM\Column(type="string", length=255,nullable=true)
   */
  private $chaturl;

  /**
   * @ORM\Column(type="string", length=255,nullable=true)
   */
  private $status;

  public function getId(){
    return $this->id;
  }

  public function getSessionid(): ?string{
    return $this->sessionid;
  }

  public function getStudent(){
  return $this->student;
}
  /**
   * @param mixed $user
   */
  public function setStudent(Users $user): void
  {
    $this->student = $user;
  }

  public function getTeacher(){
    return $this->teacher;
  }
  /**
   * @param mixed $user
   */
  public function setTeacher(Users $user): void
  {
    $this->teacher = $user;
  }

  public function getStarttime(){
    return $this->starttime;
  }

  public function setStarttime($starttime):void{
    $this->starttime = $starttime;
  }

  public function getEndtime(){
    return $this->endtime;
  }

  public function setEndtime($endtime):void{
    $this->endtime = $endtime;
  }

  public function getDuration(): ?string{
    return $this->duration;
  }

  public function setDuration($duration){
    $this->duration = $duration;
  }

  public function getRecordingurl(): ?string{
    return $this->recordingurl;
  }

  public function setRecordingurl($recordingurl){
    $this->recordingurl = $recordingurl;
  }

  public function getChaturl(): ?string{
    return $this->chaturl;
  }

  public function setChaturl($chaturl){
    $this->chaturl = $chaturl;
  }

  public function getStatus(): ?string{
    return $this->status;
  }

  public function setStatus($status){
    $this->status = $status;
  }
}
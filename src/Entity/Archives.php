<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/5/19
 * Time: 10:29 PM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArchivesRepository")
 */
class Archives
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length= 255)
   */
  private $archiveid;


  /**
   * @ORM\ManyToOne(targetEntity="App\Entity\Sessions")
   */
  private $sessionid;

  /**
   * @ORM\Column(type="datetime",nullable=true)
   */
  private $starttime;

  /**
   * @ORM\Column(type="datetime",nullable=true)
   */
  private $endtime;

  /**
   * @ORM\Column(type="string", length=100,nullable=true)
   */
  private $duration;

  /**
   * @ORM\Column(type="string", length=255,nullable=true)
   */
  private $recordingurl;

  /**
   * @ORM\Column(type="string", length=255,nullable=true)
   */
  private $status;

  public function getId(){
    return $this->id;
  }

  public function getArchiveid(){
    return $this->archiveid;
  }

  public function setArchiveid($archiveid){
    $this->archiveid = $archiveid;
  }

  public function getSessionid(){
    return $this->sessionid;
  }

  public function setSessionid(Sessions $session): void
  {
    $this->sessionid = $session;
  }


  public function getStarttime(){
    return $this->starttime;
  }

  public function setStarttime($starttime):void{
    $this->starttime = $starttime;
  }

  public function getEndtime(){
    return $this->endtime;
  }

  public function setEndtime($endtime):void{
    $this->endtime = $endtime;
  }

  public function getDuration(): ?string{
    return $this->duration;
  }

  public function setDuration($duration){
    $this->duration = $duration;
  }

  public function getRecordingurl(): ?string{
    return $this->recordingurl;
  }

  public function setRecordingurl($recordingurl){
    $this->recordingurl = $recordingurl;
  }


  public function getStatus(): ?string{
    return $this->status;
  }

  public function setStatus($status){
    $this->status = $status;
  }
}
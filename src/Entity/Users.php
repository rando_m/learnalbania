<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/4/19
 * Time: 11:37 PM
 */

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManagerAware;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $first_name;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $last_name;

  /**
   * @ORM\Column(type="string", length=50)
   */
  private $email;

  /**
   * @ORM\Column(type="string",length=100)
   */
  private $gender;

  /**
   * @ORM\Column(type="string",length=100)
   */
  private $country_residence;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $phone;

  /**
   * @ORM\Column(type="datetime")
   */
  private $dob;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $timezone;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $role;

  public function getId()
  {
    return $this->id;
  }

  public function getFirstname(): ?string
  {
    return $this->first_name;
  }

  public function setFirstname(string $first_name): self
  {
    $this->first_name = $first_name;
  }

  public function getLastname(): ?string
  {
    return $this->last_name;
  }

  public function setLastname(string $last_name): self{
    $this->last_name = $last_name;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): self
  {
    $this->email = $email;
  }

  public function getGender(): ?string
  {
    return $this->gender;
  }

  public function setGender(string $gender):self
  {
    $this->gender = $gender;
  }

  public function getCountryResidence(): ?string
  {
     return $this->country_residence;
  }

  public function setCountryResidence(string $country_residence):self
  {
    $this->country_residence = $country_residence;
  }

  public function getPhone(): ?string
  {
    return $this->phone;
  }

  public function setPhone(string $phone):self
  {
    $this->phone = $phone;
  }

  public function getDob(){
    return $this->dob;
  }

  public function setDob($dob):void{
    $this->dob = $dob;

//    return $this;
  }

  public function getTimezone(): ?string{
    return $this->timezone;
  }

  public function setTimezone($timezone){
    $this->timezone = $timezone;
  }

  public function getRole(): ?string
  {
    return $this->role;
  }

  public function setRole($role){
    $this->role = $role;
  }

}

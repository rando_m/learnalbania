<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/6/19
 * Time: 9:57 PM
 */

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenTok\OpenTok;
use OpenTok\Role;
use OpenTok\MediaMode;
use OpenTok\OutputMode;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use App\Entity\Users;
use App\Entity\Sessions;

class UsersController extends AbstractController
{

  /**
   * @Route("/teacher/session/{sessionid}", name="teacher")
   */
  public function teacher(Request $request, $sessionid){
    $canaccess = $request->cookies->get('access_teacher');
    $canaccess_student = $request->cookies->get('access_student');
    if(!empty($canaccess_student) && $canaccess_student == 'allow'){
      return $this->redirectToRoute('student_allow',array('sessionid'=>$sessionid));
    }
    if(!empty($canaccess) && $canaccess == 'allow'){
      $opentok = new OpenTok(getenv('OPENTOK_KEY'), getenv('OPENTOK_SECRET'));
      $token = $opentok->generateToken($sessionid, array(
        'role' => Role::MODERATOR
      ));
      $em = $this->getDoctrine()->getManager();
      $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
      if($session->getStatus() !='completed') {
        return $this->render('teacher.html.twig', array('apiKey' => getenv('OPENTOK_KEY'), 'sessionId' => $sessionid, 'token' => $token, 'chaturl' => $session->getChaturl(), 'user_id' => $session->getTeacher()->getId(), 'nickname' => $session->getTeacher()->getFirstname(), 'channelname' => $session->getStudent()->getFirstname() . ' - ' . $session->getTeacher()->getFirstname(), 'sendbirdkey' => getenv('SENDBIRD_KEY'),'fname'=>$session->getStudent()->getFirstname()));
      }
      else{
        return $this->redirectToRoute('teacher_allow',array('sessionid'=>$sessionid));
      }
    }
    else{
      return $this->redirectToRoute('teacher_allow',array('sessionid'=>$sessionid));
    }
  }

  /**
   * @Route("/teacher/info/{sessionid}", name="teacher_info")
   */
  public function teacherInfo($sessionid){
    $em = $this->getDoctrine()->getManager();
    $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
    if($session != null){
      $teacher = $session->getTeacher();
      if($teacher != null){
        return $this->json(['first_name'=>$teacher->getFirstname(),'last_name'=> $teacher->getLastname()]);
      }
      else{
        return $this->json(['errors' => 'Teacher does not exist']);
      }
    }
    else{
        return $this->json(['errors' => 'Session does not exist']);
    }
  }

  /**
   * @Route("/student/info/{sessionid}", name="student_info")
   */
  public function studentInfo($sessionid){
    $em = $this->getDoctrine()->getManager();
    $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
    if($session != null){
      $student = $session->getStudent();
      if($student != null){
        return $this->json(['first_name'=>$student->getFirstname(),'last_name'=> $student->getLastname()]);
      }
      else{
        return $this->json(['errors' => 'Student does not exist']);
      }
    }
    else{
      return $this->json(['errors' => 'Session does not exist']);
    }
  }

  /**
   * @Route("/student/session/{sessionid}",name="student")
   */
  public function student(Request $request,$sessionid){
    $canaccess = $request->cookies->get('access_student');
    $canaccess_teacher = $request->cookies->get('access_teacher');
    if(!empty($canaccess_teacher) && $canaccess_teacher == 'allow'){
      return $this->redirectToRoute('student_allow',array('sessionid'=>$sessionid));
    }
    if(!empty($canaccess) && $canaccess == 'allow') {
      $opentok = new OpenTok(getenv('OPENTOK_KEY'), getenv('OPENTOK_SECRET'));
      $token = $opentok->generateToken($sessionid, array('role' => Role::MODERATOR));
      $em = $this->getDoctrine()->getManager();
      $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
      if($session->getStatus() != 'completed') {
        return $this->render('student.html.twig', array('apiKey' => getenv('OPENTOK_KEY'), 'sessionId' => $sessionid, 'token' => $token, 'chaturl' => $session->getChaturl(), 'user_id' => $session->getStudent()->getId(), 'nickname' => $session->getStudent()->getFirstname(), 'channelname' => $session->getStudent()->getFirstname() . ' - ' . $session->getTeacher()->getFirstname(), 'sendbirdkey' => getenv('SENDBIRD_KEY'),'fname'=>$session->getTeacher()->getFirstname()));
      }
      else{
        return $this->redirectToRoute('student_allow',array('sessionid'=>$sessionid));
      }
    }
    else{
      return $this->redirectToRoute('student_allow',array('sessionid'=>$sessionid));
    }
  }
  /**
   * @Route("/teacher/{sessionid}", name="teacher_allow")
   */
  public function teacherAllow($sessionid,Request $request){
    $canaccess_student = $request->cookies->get('access_student');
    if(!empty($canaccess_student) && $canaccess_student == 'allow'){
      return $this->render('teacher_validate.html.twig',array('sessionId'=>$sessionid,'errors'=>'A student has already joined the classroom'));
    }
    return $this->render('teacher_validate.html.twig',array('sessionId'=>$sessionid));
  }

  /**
   * @Route("/student/{sessionid}", name="student_allow")
   */
  public function studentAllow($sessionid, Request $request){
    $canaccess_teacher = $request->cookies->get('access_teacher');
    if(!empty($canaccess_teacher) && $canaccess_teacher == 'allow'){
      return $this->render('student_validate.html.twig',array('sessionId'=>$sessionid,'errors'=>'A teacher has already joined the classroom'));
    }
    return $this->render('student_validate.html.twig',array('sessionId'=>$sessionid));
  }


  /**
   * @Route("/validate/teacher/{sessionid}",name="validate_teacher")
   */
  public function validateTeacher(Request $request, $sessionid){
    $response = new Response();
    $email = $request->request->get('email');
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository(Users::class)->findOneByEmailTeacher($email);
    $canaccess_student = $request->cookies->get('access_student');
    $response->headers->setCookie(new Cookie('access_student','disallow'));
    if(!empty($canaccess_student) && $canaccess_student == 'allow'){
      return $this->json(['errors'=>'You have already logged in as a student']);
    }

    if(empty($user)){
      $response->headers->setCookie(new Cookie('access_teacher','disallow'));
      $response->sendHeaders();
      return $this->json(['errors'=>'User does not exist']);
    }
    else{
      $session = $em->getRepository(Sessions::class)->findOneByTeacher($user,$sessionid);
      if(empty($session)){
        $response->headers->setCookie(new Cookie('access_teacher','disallow'));
        $response->sendHeaders();
        return $this->json(['errors'=> 'Session does not exist or user has not access to it']);
      }
      else{
        $response->headers->setCookie(new Cookie('access_teacher','allow'));
        $response->sendHeaders();
        return $this->json(['errors'=>'','redirect'=>$this->generateUrl('teacher',['sessionid' => $sessionid])]);

      }
    }
  }

  /**
   * @Route("/validate/student/{sessionid}",name="validate_student")
   */
  public function validateStudent(Request $request, $sessionid){
    $response = new Response();
    $email = $request->request->get('email');
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository(Users::class)->findOneByEmailStudent($email);
    $canaccess_teacher = $request->cookies->get('access_teacher');
    $response->headers->setCookie(new Cookie('access_teacher','disallow'));
    if(!empty($canaccess_teacher) && $canaccess_teacher == 'allow'){
      return $this->json(['errors'=>'You have already logged in as a teacher']);
    }

    if(empty($user)){
      $response->headers->setCookie(new Cookie('access_student','disallow'));
      $response->sendHeaders();
      return $this->json(['errors'=>'User does not exist']);
    }
    else{
      $session = $em->getRepository(Sessions::class)->findOneByStudent($user,$sessionid);
      if(empty($session)){
        $response->headers->setCookie(new Cookie('access_student','disallow'));
        $response->sendHeaders();
        return $this->json(['errors'=> 'Session does not exist or user has not access to it']);
      }
      else{
        $response->headers->setCookie(new Cookie('access_student','allow'));
        $response->sendHeaders();
        return $this->json(['errors'=>'','redirect'=>$this->generateUrl('student',['sessionid' => $sessionid])]);
      }
    }
    return $this->json(['errors'=> 'Session does not exist or user has not access to it']);
  }

  /**
   * @Route("/thank-you", name="thank_you")
   */
  public function thankYou(){
    $response = new Response();
    $response->headers->setCookie(new Cookie('access_teacher','disallow'));
    $response->headers->setCookie(new Cookie('access_student','disallow'));
    $response->sendHeaders();
    return $this->render('thankyou.twig');
  }
}
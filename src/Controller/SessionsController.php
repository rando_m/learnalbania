<?php
/**
 * Created by PhpStorm.
 * User: rando
 * Date: 3/6/19
 * Time: 11:07 PM
 */

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use OpenTok\OpenTok;
use OpenTok\Role;
use OpenTok\MediaMode;
use OpenTok\OutputMode;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Sessions;
use App\Entity\Archives;

class SessionsController extends AbstractController
{

  /**
   * @Route("/archive/start/{sessionid}", name="archive_start")
   * @param Request $request
   * @param $sessionid
   * @return Response
   */
  public function archiveStart(Request $request,$sessionid){
    $response = new Response();
    $opentok = new OpenTok(getenv('OPENTOK_KEY'), getenv('OPENTOK_SECRET'));
    $hasaudio = $request->request->get('hasAudio');
    $hasvideo = $request->request->get('hasVideo');
    $outputmode = $request->request->get('outputMode');
    $archive = $opentok->startArchive($sessionid,array(
      'name' => $sessionid,
      'hasAudio' => ($hasaudio =='on'),
      'hasVideo' => ($hasvideo == 'on'),
      'outputMode' => ($outputmode == 'composed' ? OutputMode::COMPOSED : OutputMode::INDIVIDUAL)
    ));
    $response->headers->set('Content-Type', 'application/json');
    return new Response($archive->toJson());
  }

  /**
   * @Route("/archive/stop/{sessionid}",name="archive_stop")
   */
  public function archiveStop($sessionid,Request $request){
    $response = new Response();
    $archiveid = $request->request->get('archiveid');
    if($archiveid != null) {
      try {
        $opentok = new OpenTok(getenv('OPENTOK_KEY'), getenv('OPENTOK_SECRET'));
        $archive = $opentok->stopArchive($archiveid);
      } catch (\Exception $e) {
//      $this->log->write(json_encode($e));
      }
    }
    $em = $this->getDoctrine()->getManager();
    $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
    if($session != null){
      $session->setStatus('completed');
      $em->persist($session);
      $em->flush();
    }
    $response->headers->set('Content-Type', 'application/json');
    $response->headers->setCookie(new Cookie('access_teacher','disallow'));
    $response->headers->setCookie(new Cookie('access_student','disallow'));
    $response->sendHeaders();
    if($archiveid != null)
    {
      return $this->json(['archive'=>$archive->toJson(),'redirect'=>$this->generateUrl('thank_you')]);
    }
    else{
      return $this->json(['redirect'=>$this->generateUrl('thank_you')]);
    }
  }

  /**
   * @Route("/archive/callback", name="archive_callback")
   * @param Request $request
   * @return Response
   */
  public function archiveCallback(Request $request){

    $data = (array)$request->request->all();
    $sessionid = $data['sessionId'];
    $archiveid = $data['id'];
    $status = $data['status'];
    $projectid = $data['projectId'];
    $em = $this->getDoctrine()->getManager();
    $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
    if($session != null){
      $archive = $em->getRepository(Archives::class)->findOneById($session,$archiveid);
      if($archive != null){
        if($status == 'uploaded'){
          $archive->setStarttime( \DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d h:i:s',($data['createdAt']/1000))));
          $archive->setEndtime(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d h:i:s")));
          $archive->setDuration($data['duration']);
          $archive->setStatus($status);
          $archive->setRecordingurl('https://s3.amazonaws.com/learnalbanian/'.$projectid.'/'.$archiveid.'/archive.mp4');
          $em->persist($archive);
          $em->flush();
          mail("rando@aledia.ca","My subject",'archive updated with data: '.json_encode($data));
        }
        else{
          $archive->setStarttime( \DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d h:i:s',($data['createdAt']/1000))));
          $archive->setEndtime(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d h:i:s")));
          $archive->setDuration($data['duration']);
          $archive->setStatus($status);
          $em->persist($archive);
          $em->flush();
          mail("rando@aledia.ca","My subject",'archive updated with data: '.json_encode($data));
        }

        return new Response('Archive updated');
      }
      else{
        if($status =='uploaded'){
          $newarchive = new Archives();
          $newarchive->setStarttime( \DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d h:i:s',($data['createdAt']/1000))));
          $newarchive->setEndtime(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d h:i:s")));
          $newarchive->setDuration($data['duration']);
          $newarchive->setStatus($status);
          $newarchive->setArchiveid($archiveid);
          $newarchive->setSessionid($session);
          $archive->setRecordingurl('https://s3.amazonaws.com/learnalbanian/'.$projectid.'/'.$archiveid.'/archive.mp4');
          $em->persist($newarchive);
          $em->flush();
          mail("rando@aledia.ca","My subject",'new archive with status '.$status);
        }
        else{
          $newarchive = new Archives();
          $newarchive->setStarttime( \DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d h:i:s',($data['createdAt']/1000))));
          $newarchive->setEndtime(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d h:i:s")));
          $newarchive->setDuration($data['duration']);
          $newarchive->setStatus($status);
          $newarchive->setArchiveid($archiveid);
          $newarchive->setSessionid($session);
          $em->persist($newarchive);
          $em->flush();
          mail("rando@aledia.ca","My subject",'new archive with status '.$status);
        }
        return new Response('Archive created');

      }

    }
    else{
      return new Response('Session does not exist');
    }
  }

  /**
   * @Route("/chat/start", name="chat_start", methods={"POST"})
   */
  public function chatStart(Request $request){
    $data = (array)$request->request->all();
    $sessionid =  $data['sessionId'];
    $chaturl = $data['chaturl'];
    if(isset($sessionid) && $sessionid != ''){
      $em = $this->getDoctrine()->getManager();
      $session = $em->getRepository(Sessions::class)->findOneBySession($sessionid);
      if($session != null){

        $session->setChaturl($chaturl);
        $em->persist($session);
        $em->flush();
      }
    }
    return new Response($chaturl);
  }
}